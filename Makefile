# Makefile for UtilLib

COMPONENT  ?= UtilLib

HDRS = 
OBJS = UtilLibC UtilLibAsm
CFLAGS = ${C_NO_STKCHK}
LIBRARIES = ${LIBRARY}

include CLibrary

# Dynamic dependencies:
